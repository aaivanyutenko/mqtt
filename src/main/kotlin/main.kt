import com.hivemq.client.mqtt.datatypes.MqttQos
import com.hivemq.client.mqtt.mqtt5.Mqtt5Client
import java.nio.charset.StandardCharsets
import java.util.*

fun main() {
    val client = Mqtt5Client.builder()
        .identifier(UUID.randomUUID().toString())
        .serverHost(BROKER_HOST)
        .serverPort(BROKER_PORT)
        .simpleAuth()
        .username("mqtt")
        .password("e^5I9VN[Upg+qr,Sa<20".toByteArray())
        .applySimpleAuth()
        .sslWithDefaultConfig()
        .buildAsync()
    client.connect()
    client.subscribeWith()
        .topicFilter("store/00001cd52264/command")
        .qos(MqttQos.AT_LEAST_ONCE)
        .callback { publish ->
            val byteBuffer = publish.payload.get()
            val charBuffer = StandardCharsets.UTF_8.decode(byteBuffer)
            println("request payload: $charBuffer")
        }.send()
}
