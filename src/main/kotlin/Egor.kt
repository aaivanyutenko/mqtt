import okhttp3.FormBody
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import okio.Buffer
import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

val okHttpClient = OkHttpClient.Builder()
    .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
    .build()
val app_info = "U/01.001/20220208005000"
val secret = "3fc41a35"
val timestamp = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").format(Date())
val day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
val dev_key = "5ded3b4c-1bc1-4cf1-854e-b68d4fb08912"
val request_id = UUID.randomUUID().toString()
val phone = "+375 (29) 695-79-96"
val msisdn = phone.replace("[-+\\s()]".toRegex(), "")

fun main() {
    println("app_info = ${app_info}")
    println("secret = ${secret}")
    println("Date.today.day.to_s = ${day}")
    println("dev_key = ${dev_key}")
    println("timestamp = ${timestamp}")
    println("request_id = ${request_id}")
    println("msisdn = ${msisdn}")

//    if (request_id.isNotEmpty()) return

    val bodyBuilder = FormBody.Builder()
        .add("dev_key", dev_key)
        .add("app_info", app_info)
        .add("timestamp", timestamp)
        .add("request_id", request_id)
        .add("msisdn", msisdn)
    val body = bodyBuilder.build()
    val bodyBuffer = Buffer().apply { body.writeTo(this) }
    val bodyBytes = bodyBuffer.readByteArray()
    val bodyHex = bodyBytes.joinToString("") { "%02x".format(it) }
    println("bodyHex = ${bodyHex}")
    val bodyString = String(bodyBytes)
    println("bodyString = $bodyString")

    val sha256 = MessageDigest.getInstance("SHA-256")
    val key = sha256.digest("$app_info$secret$day".toByteArray())
    val keyString = key.joinToString("") { "%02x".format(it) }
    val secretKey = SecretKeySpec(keyString.toByteArray(), "HmacSHA256")
    val sha256Hmac = Mac.getInstance("HmacSHA256")
    sha256Hmac.init(secretKey)
    println("keyString = ${keyString}")
    println("secretKey = ${secretKey.encoded.joinToString("") { "%02x".format(it) }}")
    val sign = sha256Hmac.doFinal(bodyBytes).joinToString("") { "%02x".format(it) }
    println("sign = ${sign}")

    val request = Request.Builder()
        .url("https://parkouka.by/api/app/session/login?sign=$sign")
        .post(body).build()
    val response = okHttpClient.newCall(request).execute()
    val responseString = response.body?.string()
    println("response = ${responseString}")

    val sha256HmacControl = Mac.getInstance("HmacSHA256")
    val secretKeyControl = SecretKeySpec(keyString.toByteArray(), "HmacSHA256")
    sha256HmacControl.init(secretKeyControl)
    val signControl = sha256HmacControl.doFinal(bodyString.toByteArray()).joinToString("") { "%02x".format(it) }
    println("signControl = ${signControl}")
}
