import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*
import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec


val executor: ExecutorService = Executors.newSingleThreadExecutor()

fun calculate(input: Int): Future<Int> =
    executor.submit(Callable {
        Thread.sleep(500)
        return@Callable input * input
    })

fun main() {
    val feature = calculate(3)
    val time = System.currentTimeMillis()
    println(feature.get())
    println(System.currentTimeMillis() - time)
    println("feature.isDone = ${feature.isDone}")
    executor.shutdown()
    println(convertStringToHash("APuq#bH2Gk3"))
    println("8XvPsRb6DqISwlBM5u9oQT7Cr2PrM5rFgm8OV10OJUJKzXGV9ijGf0TzUp3kz1WibChGZXfmGANfQ/S3LWfc4Q==")
    println(hashPasswordByteMe("APuq#bH2Gk3"))
    println("f7351bb5e80335808bbf6446c096e23397f9c571ab499874415b8b81b18fed21")
    println(hashPasswordByteMe(convertStringToHash("APuq#bH2Gk3")))
}

fun hashPasswordByteMe(input: String): String {
    val hmac = Mac.getInstance("HmacSHA256")
    val secret = SecretKeySpec("ElTTftT3Tx5QYMGc3mvg".toByteArray(), "HmacSHA256")
    hmac.init(secret)
    val digest = hmac.doFinal(input.toByteArray())
    return String.format("%064x", BigInteger(1, digest))
}

fun convertStringToHash(input: String): String {
    try {
        val messageDigest = MessageDigest.getInstance("SHA-512")
        val encoded = messageDigest.digest(input.toByteArray())
        val result: String = Base64.getEncoder().encodeToString(encoded)
        return result.replace("\n", "")
    } catch (e: NoSuchAlgorithmException) {
        e.printStackTrace()
    }
    return ""
}