import java.io.File

fun main() {
    val file = File("install_watchdog.sh")
    val bytes = file.readBytes()
    bytes.forEach(::println)
}