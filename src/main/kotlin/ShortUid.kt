import java.util.*

fun main() {
    val uid = UUID.randomUUID().toString().dropWhile { it != '-' }.dropLastWhile { it != '-' }.drop(1).dropLast(1)
    println(uid)
}