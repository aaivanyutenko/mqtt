import io.reactivex.Observable
import java.text.SimpleDateFormat
import java.util.*

object Hello {
    @JvmStatic
    fun main(args: Array<String>) {
        val observable = Observable.create<String> { emitter ->
            println("emitter = $emitter")
        }
        observable.subscribe()
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_MONTH, 3)
        val format = SimpleDateFormat("dd.MM.yyyy")
        var rows = 0
        while (rows < 1104) {
            val day = calendar.get(Calendar.DAY_OF_WEEK)
            if (day !in setOf(1, 7)) {
                println(format.format(calendar.time))
                rows++
            }
            calendar.add(Calendar.DAY_OF_MONTH, 1)
        }
    }
}