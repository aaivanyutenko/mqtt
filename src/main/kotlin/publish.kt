import com.hivemq.client.mqtt.datatypes.MqttQos
import com.hivemq.client.mqtt.mqtt5.Mqtt5Client
import java.io.File
import java.util.*

fun main() {
    val hello = File("mqtt")
    hello.writeBytes(Base64.getEncoder().encode("e^5I9VN[Upg+qr,Sa<20".toByteArray()))
    val world = hello.readText()
    println(world)
    println(String(Base64.getEncoder().encode("test".toByteArray())))
    val update = """
        {
            "data": {
                "url":"http://34.105.17.28:3000/ps"
            },
            "command": "${GCMConstants.GCM_ACTION_UPDATE_PANTRY_SERVICE}",
            "sentTimeMs": ${System.currentTimeMillis()}
        }
    """.trimIndent()
    val screenshot = """
        {
            "data": {
                "email":"anton.ivanyutenko@gmail.com"
            },
            "command": "${GCMConstants.GCM_ACTION_TAKE_SCREENSHOT}",
            "sentTimeMs": ${System.currentTimeMillis()}
        }
    """.trimIndent()
    val client = Mqtt5Client.builder()
        .identifier(UUID.randomUUID().toString())
        .serverHost(BROKER_HOST)
        .serverPort(BROKER_PORT)
        .sslWithDefaultConfig()
        .simpleAuth().username("mqtt").password("e^5I9VN[Upg+qr,Sa<20".toByteArray()).applySimpleAuth()
        .buildBlocking()
//    val ack = client.connect()
//    println("ack = ${ack}")
//        .buildAsync()
    client.connect()
    client.publishWith()
        .topic("store/00001d9b44a0/command") // Michael's one more machine
//        .topic("store/00001cd52264/command")
//        .topic("store/00001cd525f6/command") // Michael's machine
        .qos(MqttQos.AT_LEAST_ONCE)
        .payload(update.toByteArray())
        .responseTopic("hello")
        .correlationData("world".toByteArray())
        .send()
    client.disconnect()
//        .thenCompose {
//            client.publishWith()
//                .topic("store/00001cd52264/command")
//                .payload(payload.toByteArray())
//                .send()
//        }.thenCompose { client.disconnect() }
}
