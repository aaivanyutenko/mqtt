/**
 * List of constants used by GCM.
 *
 * @author Alexander Tyschenko (Stanfy - http://www.stanfy.com)
 */
public final class GCMConstants {

  /** Sender identifier. // TODO should be moved to config file. */
  public static final String GCM_SENDER_ID = "329198057452";


  /** Opens the kiosk door. */
  public static final String GCM_ACTION_OPEN_DOOR = "open_door";
  /** Closes the kiosk door. */
  public static final String GCM_ACTION_CLOSE_DOOR = "close_door";
  /** Signals the kiosk to re-download product information from the server, including price, image, etc. */
  public static final String GCM_ACTION_DOWNLOAD_PRODUCTS = "download_products";
  /** Force stop PantryService app. */
  public static final String GCM_ACTION_STOP_PANTRY_SERVICE = "stop_app";
  /** Starts PantryService app. */
  public static final String GCM_ACTION_START_PANTRY_SERVICE = "start_app";
  /** Forces stop PantryService app and then starts it. */
  public static final String GCM_ACTION_RESTART_PANTRY_SERVICE = "restart_app";
  /** Hides buttons bar at the bottom of the screen. */
  public static final String GCM_ACTION_HIDE_BUTTONS_BAR = "hide_bar";
  /** Shows buttons bar at the bottom of the screen. */
  public static final String GCM_ACTION_SHOW_BUTTONS_BAR = "show_bar";
  /** Reboots tablet. */
  public static final String GCM_ACTION_REBOOT_TABLET = "reboot";
  /** Indicates to reboot tablet now despite of state of PantryService. */
  public static final String GCM_PAYLOAD_FORCE = "force";
  /** Powers off the tablet. */
  public static final String GCM_ACTION_POWER_OFF = "power_off";
  /** Launches PantryWatchdog. */
  public static final String GCM_ACTION_SHOW_WATCHDOG = "show_admin_panel";
  /** Updates PantryService. */
  public static final String GCM_ACTION_UPDATE_PANTRY_SERVICE = "update_pantry_service";
  /** */
  public static final String GCM_ACTION_DOWNLOAD_CMAPP = "download_cmapp";
  /** Updates PantryWatchdog. */
  public static final String GCM_ACTION_UPDATE_WATCHDOG = "update_watchdog";
  /** Sends the current reading of all tags in the kiosk to the email address in the payload. */
  public static final String GCM_ACTION_GET_CURRENT_RSSI = "get_current_rssi";
  /** Sends the PepWave serial number, if available, to the email address in the payload. */
  public static final String GCM_ACTION_GET_PEPWAVE_SERIAL = "get_pepwave_serial";
  /** Locks down the kiosk. If there is a custom message specified in the payload, that message will be displayed on the screen. */
  public static final String GCM_ACTION_LOCK_KIOSK = "lock_kiosk";
  /** Remove the lock-down on the kiosk. */
  public static final String GCM_ACTION_UNLOCK_KIOSK = "unlock_kiosk";
  /** Signals the kiosk to send its current inventory to the backend. */
  public static final String GCM_ACTION_SEND_INVENTORY = "send_inventory";
  /** Signals the kiosk to send its current status to the backend. */
  public static final String GCM_ACTION_SEND_STATUS = "send_status";
  /** Signals the kiosk to send logs. */
  public static final String GCM_ACTION_SEND_LOGS = "send_logs";
  /** Calibrates the kiosk temperature according to the number specified in the payload. */
  public static final String GCM_ACTION_CALIBRATE_TEMPERATURE = "calibrate_temperature";
  /** Deletes the current inventory stored in the PantryService database table and rebuilds it. */
  public static final String GCM_ACTION_DROP_DATABASE = "drop_database";
  /** Takes a screenshot of the tablet and sends it to the email address in the payload. */
  public static final String GCM_ACTION_TAKE_SCREENSHOT = "take_screenshot";
  /** Signals the kiosk to re-download the manager data of the kiosk, such as new login credentials. */
  public static final String GCM_ACTION_UPDATE_MANAGER_DATA = "update_manager_data";
  /** Turns on tablet WiFi. */
  public static final String GCM_ACTION_ENABLE_WIFI = "enable_wifi";
  /** Turns off tablet WiFi. */
  public static final String GCM_ACTION_DISABLE_WIFI = "disable_wifi";
  /** Turns off and turns on tablet WiFi. */
  public static final String GCM_ACTION_RESET_WIFI = "reset_wifi";
  /** Override offline duration using time in minutes in payload. */
  public static final String GCM_ACTION_OFFLINE_MINUTES = "offline_minutes";

  /** RSSI threshold to add an item to inventory. */
  public static final String GCM_ACTION_RSSI_THRESHOLD = "hack_rssi_threshold";
  /** RSSI threshold to add an item to inventory when adding inventory item is allowed only in restocking mode. */
  public static final String GCM_ACTION_RSSI_THRESHOLD_WHEN_ADD_ITEM_ONLY_IN_RESTOCK = "hack_rssi_threshold_when_add_item_only_in_restock";
  /** Wipe all data from sdcard and app storage **/
  public static final String GCM_ACTION_WIPE_ALL_DATA = "wipe_all_data";
  /** Test message to check GCM **/
  public static final String GCM_ACTION_TEST = "test_gcm";

  public static final String[] AVAILABLE_COMMANDS = {
    GCM_ACTION_OPEN_DOOR,
    GCM_ACTION_CLOSE_DOOR,
    GCM_ACTION_DOWNLOAD_PRODUCTS,
    GCM_ACTION_STOP_PANTRY_SERVICE,
    GCM_ACTION_START_PANTRY_SERVICE,
    GCM_ACTION_RESTART_PANTRY_SERVICE,
    GCM_ACTION_HIDE_BUTTONS_BAR,
    GCM_ACTION_SHOW_BUTTONS_BAR,
    GCM_ACTION_REBOOT_TABLET,
    GCM_ACTION_POWER_OFF,
    GCM_ACTION_SHOW_WATCHDOG,
    GCM_ACTION_UPDATE_PANTRY_SERVICE,
    GCM_ACTION_DOWNLOAD_CMAPP,
    GCM_ACTION_UPDATE_WATCHDOG,
    GCM_ACTION_GET_CURRENT_RSSI,
    GCM_ACTION_GET_PEPWAVE_SERIAL,
    GCM_ACTION_LOCK_KIOSK,
    GCM_ACTION_UNLOCK_KIOSK,
    GCM_ACTION_SEND_INVENTORY,
    GCM_ACTION_SEND_STATUS,
    GCM_ACTION_SEND_LOGS,
    GCM_ACTION_CALIBRATE_TEMPERATURE,
    GCM_ACTION_DROP_DATABASE,
    GCM_ACTION_TAKE_SCREENSHOT,
    GCM_ACTION_UPDATE_MANAGER_DATA,
    GCM_ACTION_ENABLE_WIFI,
    GCM_ACTION_DISABLE_WIFI,
    GCM_ACTION_RESET_WIFI,
    GCM_ACTION_OFFLINE_MINUTES,
    GCM_ACTION_RSSI_THRESHOLD,
    GCM_ACTION_RSSI_THRESHOLD_WHEN_ADD_ITEM_ONLY_IN_RESTOCK,
    GCM_ACTION_WIPE_ALL_DATA,
    GCM_ACTION_TEST,
  };


  private GCMConstants() { }
}
