from flask import Flask
from flask import send_file
from datetime import datetime

app = Flask(__name__)

@app.route('/')
def index():
    now = datetime.now()
    dt_string = now.strftime("%d.%m.%Y %H:%M:%S")
    return 'Hello world: ' + dt_string

@app.route('/ps')
def hello():
    return send_file('pantry-service-update.apk', attachment_filename='pantry-service.apk')

@app.route('/pw')
def world():
    return send_file('pantry-watchdog-update.apk', attachment_filename='pantry-watchdog.apk')

app.run(host='0.0.0.0', port=3000)