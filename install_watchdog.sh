#!/bin/bash

# Uninstall current PantryWatchdog application
pm uninstall com.pantrylabs.watchdog

# Install new PantryWatchdog from apk which was downloaded
pm install -r -d $1

# Launch PantryWatchdog activity with category LAUNCHER
am start -n com.pantrylabs.watchdog/com.pantrylabs.watchdog.activity.WatchdogActivity

# Stop PantryService application
am force-stop me.pantre.app

# Launch PantryService activity with category LAUNCHER
am start me.pantre.app/me.pantre.app.ui.activity.UserActivity

# Clean up install script
rm /sdcard/install_watchdog.sh